package ru.technolab.demo;

import java.io.PrintWriter;
import java.io.StringWriter;

import org.junit.Test;

public class AbstractTests {

	@Test
	public void testPrintStackTrace() {
	  StringWriter sw = new StringWriter();
	  PrintWriter pw = new PrintWriter(sw);
	  new Exception().printStackTrace(pw);
	  System.out.println(sw.toString());
	}
}
